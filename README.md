# [OpenCL](https://www.khronos.org/opencl/)

So I decided to let my GPU do some work ...

Right now I am just using the typical tutorial example, which adds to vectors.


## [JogAmp JOCL](https://jogamp.org/jogl/www/)

Since there seem to be two OpenCL implementations for Java with the same name JOCL, I want to clarify, that I tried the JogAmp JOCL (`org.jogamp.jocl`) and not the other one (`org.jocl`).

* pro
    * This thing is fast! On my GPU more than 12 times faster than the JavaCL example.
* contra
    * Readability is a little poor
    * I guess, I have to understand OpenCL a bit better to handle this one


## [JavaCL](https://github.com/nativelibs4java/JavaCL)

* pro
    * Easy to read
    * Smaller programs
    * (There's even a code generator for type-safe access to CL programs, didn't try it though)
* contra
    * Seems a lot slower than JOCL


## Summary

An interesting [question at Stackoverflow](http://stackoverflow.com/questions/4649951/how-do-javacl-and-jogamp-jocl-compare/) is discussing a comparison. The thread is quite old, but at least for me the results are still valid.

## Links

* Very helpful explanation: ["A Gentle Introduction to OpenCL"](http://www.drdobbs.com/parallel/a-gentle-introduction-to-opencl/231002854)



https://github.com/skeeto/perlin-noise/blob/opencl/src/com/nullprogram/noise/perlin3d.cl