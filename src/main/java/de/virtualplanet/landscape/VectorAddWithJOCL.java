package de.virtualplanet.landscape;

import static com.jogamp.opencl.CLMemory.Mem.READ_ONLY;
import static com.jogamp.opencl.CLMemory.Mem.WRITE_ONLY;
import static java.lang.Math.min;
import static java.lang.System.nanoTime;
import static java.lang.System.out;

import java.io.IOException;
import java.nio.FloatBuffer;

import org.apache.commons.lang3.RandomUtils;

import com.jogamp.opencl.CLBuffer;
import com.jogamp.opencl.CLCommandQueue;
import com.jogamp.opencl.CLContext;
import com.jogamp.opencl.CLDevice;
import com.jogamp.opencl.CLKernel;
import com.jogamp.opencl.CLProgram;

/**
 * Source: https://jogamp.org/wiki/index.php/JOCL_Tutorial
 */
public class VectorAddWithJOCL {

	public static void main(final String[] args) throws IOException {

		// set up (uses default CLPlatform and creates context for all devices)
		CLContext context = CLContext.create();

		// always make sure to release the context under all circumstances
		// not needed for this particular sample but recommended
		try {

			// select fastest device
			CLDevice device = context.getMaxFlopsDevice();
			out.println("using " + device);

			// Length of arrays to process
			int elementCount = 1444477;
			// Local work size dimensions
			int localWorkSize = min(device.getMaxWorkGroupSize(), 256);
			// rounded up to the nearest multiple of the localWorkSize
			int globalWorkSize = roundUp(localWorkSize, elementCount);

			// A, B are input buffers, Out is for the result
			CLBuffer<FloatBuffer> bufferA = context.createFloatBuffer(
					globalWorkSize, READ_ONLY);
			CLBuffer<FloatBuffer> bufferB = context.createFloatBuffer(
					globalWorkSize, READ_ONLY);
			CLBuffer<FloatBuffer> bufferOut = context.createFloatBuffer(
					globalWorkSize, WRITE_ONLY);

			out.println("used device memory: "
					+ (bufferA.getCLSize() + bufferB.getCLSize() + bufferOut
							.getCLSize()) / 1000000 + "MB");

			// fill input buffers with random numbers
			fillBuffer(bufferA.getBuffer());
			fillBuffer(bufferB.getBuffer());

			// load sources, create and build program
			CLProgram program = context.createProgram(
					VectorAddWithJOCL.class.getClassLoader()
							.getResourceAsStream("VectorAdd.cl")).build();

			// get a reference to the kernel function with the name 'VectorAdd'
			// and map the buffers to its input parameters.
			CLKernel kernel = program.createCLKernel("VectorAdd");
			kernel.putArgs(bufferA, bufferB, bufferOut).putArg(elementCount);

			// create command queue on device.
			CLCommandQueue queue = device.createCommandQueue();

			// asynchronous write of data to GPU device,
			// followed by blocking read to get the computed results back.
			long time = nanoTime();
			queue.putWriteBuffer(bufferA, false).putWriteBuffer(bufferB, false)
					.put1DRangeKernel(kernel, 0, globalWorkSize, localWorkSize)
					.putReadBuffer(bufferOut, true);
			time = nanoTime() - time;

			// print first few elements of the resulting buffer to the console.
			for (int i = 0; i < 10; i++) {
				out.print(bufferOut.getBuffer().get() + ", ");
			}
			out.println("...; " + bufferOut.getBuffer().remaining() + " more");

			out.println("computation took: " + (time / 1000000d) + "ms");

		} finally {
			// cleanup all resources associated with this context.
			context.release();
		}

	}

	private static void fillBuffer(final FloatBuffer buffer) {
		while (buffer.remaining() != 0)
			buffer.put(RandomUtils.nextFloat(0, 100));
		buffer.rewind();
	}

	private static int roundUp(final int groupSize, final int globalSize) {
		int r = globalSize % groupSize;
		if (r == 0) {
			return globalSize;
		} else {
			return globalSize + groupSize - r;
		}
	}

}
