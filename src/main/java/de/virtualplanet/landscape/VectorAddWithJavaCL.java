package de.virtualplanet.landscape;

import static java.lang.System.nanoTime;
import static org.bridj.Pointer.allocateFloats;

import java.io.IOException;
import java.nio.ByteOrder;

import org.apache.commons.lang3.RandomUtils;
import org.bridj.Pointer;

import com.nativelibs4java.opencl.CLBuffer;
import com.nativelibs4java.opencl.CLContext;
import com.nativelibs4java.opencl.CLEvent;
import com.nativelibs4java.opencl.CLKernel;
import com.nativelibs4java.opencl.CLMem.Usage;
import com.nativelibs4java.opencl.CLProgram;
import com.nativelibs4java.opencl.CLQueue;
import com.nativelibs4java.opencl.JavaCL;
import com.nativelibs4java.util.IOUtils;

/**
 * Source: https://code.google.com/p/javacl/wiki/GettingStarted
 */
public class VectorAddWithJavaCL {

	public static void main(final String[] args) throws IOException {

		// set up context
		CLContext context = JavaCL.createBestContext();
		ByteOrder byteOrder = context.getByteOrder();

		int n = 1444477;

		// Create OpenCL input buffers, and output buffer:
		CLBuffer<Float> bufferA = context.createBuffer(Usage.Input,
				getRandomBuffer(n, byteOrder));
		CLBuffer<Float> bufferB = context.createBuffer(Usage.Input,
				getRandomBuffer(n, byteOrder));
		CLBuffer<Float> bufferOut = context.createBuffer(Usage.Output,
				Float.class, n);

		// load sources, create and build program
		String src = IOUtils.readText(VectorAddWithJavaCL.class.getClassLoader()
				.getResource("VectorAdd.cl"));
		CLProgram program = context.createProgram(src);

		// get a reference to the kernel function with the name 'VectorAdd'
		// and map the buffers to its input parameters.
		CLKernel addFloatsKernel = program.createKernel("VectorAdd");
		addFloatsKernel.setArgs(bufferA, bufferB, bufferOut, n);

		// create command queue
		CLQueue queue = context.createDefaultQueue();

		long time = nanoTime();
		CLEvent addEvt = addFloatsKernel.enqueueNDRange(queue, new int[] { n });

		Pointer<Float> outPtr = bufferOut.read(queue, addEvt); // blocks until
																// add_floats
																// finished
		time = nanoTime() - time;

		// Print the first 10 output values :
		for (int i = 0; i < 10 && i < n; i++)
			System.out.print(outPtr.get(i) + ", ");

		System.out.println();
		System.out.println("computation took: " + (time / 1000000d) + "ms");
	}

	private static Pointer<Float> getRandomBuffer(final int count,
			final ByteOrder byteOrder) {
		Pointer<Float> aPtr = allocateFloats(count).order(byteOrder);
		for (int i = 0; i < count; i++) {
			float value = RandomUtils.nextFloat(0, 100);
			aPtr.set(i, value);
		}
		return aPtr;
	}
}
